# Ufurmix-pawprints [OwO]

It's a theme for plymouth (boot splash for linux) taked from the Ubuntu Furry Remix made by Shnatsel.
The purpose of making this fork was because i like the theme OwO also i'm proud to be a furry.

source: [https://launchpad.net/ufurmix](https://launchpad.net/ufurmix)

![Ufurmix-pawprints](preview.gif "Ufurmix-pawprints")

## First time using Plymouth? [>w>]

If this is your first time using plymouth, first install Plymouth on your distro

### Debian/Ubuntu
```
sudo apt install plymouth
```
### Fedora/Red Hat
```
sudo dnf install plymouth
```
### ArchLinux/Manjaro/Artix
```
sudo pacman -S plymouth
```

Add plymouth to the HOOKS array in mkinitcpio.conf. It must be added after base and udev for it to work:

```
/etc/mkinitcpio.conf
______________________________

HOOKS=(base udev plymouth ...)
```

Now you need to add your (kernel) graphics driver to your initramfs.
- Intel `i915`
- AMD `radeon`
- Nvidia `nouveau`

For Example:

```
/etc/mkinitcpio.conf
______________________________

MODULES=(i915 ...)
```
Check your mkinitcpio here

```
ls /etc/mkinitcpio.d/
______________________________

linux.preset  linux-zen.preset
```

Now you can run this comand using your kernel (for my case, I'll do it with my Linux-zen kernel)

```
sudo mkinitcpio -p linux-kernel
```

## Installation pawprints [OwO]

It's necessary use root for install the theme.
```
sudo make install
```

## Uninstallation pawprints [OnO]

It's necessary use root for uninstall the theme. This delete the theme and changeto `pinner theme`
```
sudo make uninstall
```
