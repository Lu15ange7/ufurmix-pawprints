all: install

welcome:
	@echo '								'
	@echo '---------------------------------------------------------'
	@echo '								'
	@echo '		  ⣠⣾⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣿⣿⣿⣿⣷⣄		'
	@echo '		  ⣿⣿⡇⠀⠀⢸⣿⢰⣿⡆⠀⣾⣿⡆⠀⣾⣷⠀⣿⣿⡇⠀⠀⢸⣿⣿		'
	@echo '		  ⣿⣿⡇⠀⠀⢸⣿⠘⣿⣿⣤⣿⣿⣿⣤⣿⡇⠀⢻⣿⡇⠀⠀⢸⣿⣿		'
	@echo '		  ⣿⣿⡇⠀⠀⢸⡿⠀⢹⣿⣿⣿⣿⣿⣿⣿⠁⠀⢸⣿⣇⠀⠀⢸⣿⣿		'
	@echo '		  ⠙⢿⣷⣶⣶⡿⠁⠀⠈⣿⣿⠟⠀⣿⣿⠇⠀⠀⠈⠻⣿⣿⣿⣿⡿⠋		'
	@echo '								'
	@echo '		   Plymouth > ufurmix-pawprints'
	@echo '		this theme was made by by Shnatsel'
	@echo '		for the proyect Ubuntu Furry Remix'
	@echo '		       Forked by Lu15ange7'
	@echo '								'
	@echo '---------------------------------------------------------'
	@echo '								'


install: welcome
	@cp -r ufurmix-pawprints/ /usr/share/plymouth/themes/
	@cp plymouthd.defaults /usr/share/plymouth/
	@sudo plymouth-set-default-theme -R ufurmix-pawprints
	@echo '								'
	@echo " ✓ ufurmix-pawprints installed"
	@echo '								'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '								'
	@echo '---------------------------------------------------------'
	@echo '								'


uninstall: welcome
	@rm -r /usr/share/plymouth/themes/ufurmix-pawprints
	@sudo plymouth-set-default-theme -R spinner
	@echo '								'
	@echo " ✓ ufurmix-pawprints uninstalled"
	@echo '								'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '								'
	@echo '---------------------------------------------------------'
	@echo '								'
